(**
This file is part of the coq-teach library

Copyright (C) Boldo, Clément, Hamelin, Mayero, Rousselin

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 3 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
COPYING file for more details.
*)
From Coq Require Import ZArith.BinInt.
From Teach Require Import Pos_Compl.
Module Pos.
Local Open Scope positive_scope.
Definition primeb_pos_aux (p : positive) (fuel : positive) : bool.
Proof.
  induction fuel as [| fuel' IH'] using Pos.peano_rect.
  - exact true.
  - destruct (Z.pos p mod (Z.pos (Pos.succ fuel')) =? 0)%Z eqn:E.
    + exact false.
    + exact IH'.
Defined.

Lemma primeb_pos_aux_succ (p : positive) (fuel : positive) :
  primeb_pos_aux p (Pos.succ fuel) =
  if (Z.pos p mod (Z.pos (Pos.succ fuel)) =? 0)%Z then false else
    primeb_pos_aux p fuel.
Proof. 
  unfold primeb_pos_aux;
  rewrite Pos.peano_rect_succ;
  destruct (Z.pos p mod (Z.pos (Pos.succ fuel)) =? 0)%Z eqn:E; reflexivity.
Qed.

Lemma primeb_pos_aux_correct (p : positive) (fuel : positive) :
  primeb_pos_aux p fuel = true <->
    forall (n : positive), (2 <= n <= fuel)%positive ->
    (Z.pos p mod (Z.pos n) =? 0)%Z = false.
Proof.
  split; induction fuel as [| fuel' IH'] using Pos.peano_rect.
  - intros _ n [C1 C2]; exfalso.
    pose proof (Pos.le_trans _ _ _ C1 C2) as C.
    unfold Pos.le in C; apply C; reflexivity.
  - intros H n [In In'].
    rewrite primeb_pos_aux_succ in H.
    destruct (Z.pos p mod (Z.pos (Pos.succ fuel')) =? 0)%Z eqn:E; simpl in H.
    + discriminate H.
    + apply Pos.le_succ_r in In' as [In' | En].
      * apply IH'; [exact H |]; split; assumption.
      * rewrite En; exact E.
  - intros _; reflexivity.
  - intros H.
    rewrite primeb_pos_aux_succ.
    destruct (Z.pos p mod (Z.pos (Pos.succ fuel')) =? 0)%Z eqn:E; simpl.
    + rewrite <-E; rewrite H; [reflexivity |]; split; [| exact (Pos.le_refl _)].
      exact (Pos.le_2_succ _).
    + apply IH'; intros n [In In']; apply H; split; [exact In |].
      apply Pos.le_le_succ_r; exact In'. 
Qed.
End Pos.
