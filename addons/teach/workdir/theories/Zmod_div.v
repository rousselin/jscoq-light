(**
This file is part of the coq-teach library

Copyright (C) Boldo, Clément, Hamelin, Mayero, Rousselin

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 3 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
COPYING file for more details.
*)

From Coq.ZArith Require Import BinInt Wf_Z.

(** ** Missing lemmas about modular arithmetics in Z *)

Module Z.
#[local] Open Scope Z.
(** Below are lemmas either missing, or with unnecessary hypotheses, or wrong
name in the standard library. *)

Lemma div_eucl_0_r (a : Z) : Z.div_eucl a 0 = (0, a).
Proof. destruct a as [| a | a]; reflexivity. Qed.

Lemma div_eucl_0_l (b : Z) : Z.div_eucl 0 b = (0, 0).
Proof. reflexivity. Qed.

(* Alternative: Zdiv.Zmod_0_r *)
Lemma mod_0_r (a : Z) : a mod 0 = a.
Proof.
  pose proof (div_eucl_0_r a) as E.
  unfold "mod"; rewrite E; reflexivity.
Qed.

(* Alternative: Zdiv.Zdiv_0_r *)
Lemma div_0_r (a : Z) : a / 0 = 0.
Proof.
  pose proof (div_eucl_0_r a) as E.
  unfold "/"; rewrite E; reflexivity.
Qed.

(* Alternative: Zdiv.Zmod_0_l, better than Z.mod_0_l *)
Lemma mod_0_l (b : Z) : 0 mod b = 0.
Proof.
  pose proof (div_eucl_0_l b) as E.
  unfold "mod"; rewrite E; reflexivity.
Qed.

(* Alternative: Zdiv.Zdiv_0_l, better than Z.div_0_l *)
Lemma div_0_l (b : Z) : 0 / b = 0.
Proof.
  pose proof (div_eucl_0_l b) as E.
  unfold "/"; rewrite E; reflexivity.
Qed.

(* Better version of Z.div_mod *)
Lemma div_mod (a b : Z) : a = b * (a / b) + a mod b.
Proof.
  destruct (Z.eq_dec b 0) as [-> | E].
  - rewrite div_0_r, mod_0_r; reflexivity.
  - exact (Z.div_mod a b E).
Qed.

(* Better version of Z.mod_mul, better name than Zdiv.Z_mod_mult
    Note that (a * b) / b = a is not valid in general if b = 0. *)
Lemma mod_mul (a b : Z) : (a * b) mod b = 0.
Proof.
  pose proof (div_mod (a * b) b) as eq.
  destruct (Z.eq_dec b 0) as [-> | Eb].
  - rewrite mod_0_r, Z.mul_0_r; reflexivity.
  - rewrite Z.div_mul, (Z.mul_comm b a) in eq by (exact Eb).
    rewrite <-(Z.add_0_r (a * b)) in eq at 1.
    apply Z.add_reg_l in eq; symmetry; exact eq.
Qed.

(* Better version of Znumtheory.Zmod_divide and Z.mod_divide, both have
   the extra hypothesis [(b <> 0)], Z.mod_divide is an equivalence. *)
Lemma mod_divide (a b : Z) : a mod b = 0 <-> (b | a).
Proof.
  pose proof (div_mod a b) as eq; split.
  - intros H; rewrite H, Z.add_0_r, Z.mul_comm in eq; exists (a / b); exact eq.
  - intros [q ->]; exact (mod_mul _ _).
Qed.

(* Better version of Z.add_mod *)
Lemma add_mod (a b n : Z) : (a + b) mod n = (a mod n + b mod n) mod n.
Proof.
  destruct (Z.eq_dec n 0) as [-> | H].
  - rewrite !mod_0_r; reflexivity.
  - exact (Z.add_mod a b n H).
Qed.

(* NOTE: Z.mul_mod but without the unnecessary hypothesis *)
Lemma mul_mod (a b n : Z) : (a * b) mod n = (a mod n * (b mod n)) mod n.
Proof.
  destruct (Z.eq_dec n 0) as [-> | H]; [now rewrite !Z.mod_0_r |].
  exact (Z.mul_mod a b n H).
Qed.

(* NOTE: Z.mul_mod_idemp_r but without the unnecessary hypothesis *)
Lemma mul_mod_idemp_r (a b n : Z) : (a * (b mod n)) mod n = (a * b) mod n.
Proof.
  destruct (Z.eq_dec n 0) as [-> | H].
  - rewrite !Z.mod_0_r; reflexivity.
  - rewrite Z.mul_mod_idemp_r by (exact H); reflexivity.
Qed.

(* Currently not in the stdlib *)
Lemma pow_mod (a k n : Z) : (a ^ k) mod n = ((a mod n) ^ k) mod n.
Proof.
  destruct (Z.neg_nonneg_cases k).
  - rewrite 2Z.pow_neg_r by (assumption); reflexivity.  (* <- ugly *)
  - apply (Wf_Z.natlike_ind (fun k => a ^ k mod n = (a mod n) ^ k mod n)); (* <- ugly *)
      [rewrite 2Z.pow_0_r; reflexivity | | exact H].
    intros x Hx IH; rewrite 2Z.pow_succ_r, mul_mod, IH by (assumption).
    rewrite mul_mod_idemp_r; reflexivity.
Qed.

(* Currently does not exist in the stdlib, note the unfortunate condition *)
Lemma pow_m1_l_odd (n : Z) : (0 <= n) -> Z.Odd n -> (- 1) ^ n = - 1.
Proof.
  intros H H'.
  replace (- 1) with (- (1)) by reflexivity. (* <- ugly *)
  rewrite Z.pow_opp_odd by (exact H').
  rewrite Z.pow_1_l by exact H; reflexivity.
Qed.

Lemma gcd_ge_1 (a b : Z) : a <> 0 \/ b <> 0 -> 1 <= Z.gcd a b.
Proof.
  intros Hab; rewrite Z.one_succ; apply Z.le_succ_l, Z.le_neq; split.
  - exact (Z.gcd_nonneg _ _).
  - intros [Ha Hb]%eq_sym%Z.gcd_eq_0; destruct Hab as [Ha' | Hb'];
      [exact (Ha' Ha) | exact (Hb' Hb)].
Qed.

Lemma gt_1_neq_0 (n : Z) : 1 < n -> n <> 0.
Proof.
  intros H1 H2; apply (Z.lt_asymm 1 n); [exact H1 | rewrite H2; exact Z.lt_0_1].
Qed.

Lemma ge_1_gt_0 (n : Z) : 1 <= n -> 0 < n.
Proof. rewrite Z.one_succ; intros H%Z.le_succ_l; exact H. Qed.

Lemma gt_1_gt_0 (n : Z) : 1 < n -> 0 < n.
Proof. intros H; apply Z.lt_trans with (1 := Z.lt_0_1); exact H. Qed.

(* Equivalence in Z.divide_add_cancel_r *)
(* NOTE: the stdlib is inconsistent in the naming of "cancel_l" or "cancel_r"
lemmas, most of the times "l" or "r" refers to the position of the canceled
operand, but sometimes (see [Z.divide_add_cancel_r]) it refers to the position
of the uncanceled operand.
See also the ouput of [Search "cancel_r"].
We chose "l" or "r" to denote the position of the canceled operand as we believe
it's probably what the user expects. *)

Lemma divide_add_cancel_l (n m p : Z) : (n | m) -> (n | m + p) <-> (n | p).
Proof.
  intros H; split.
  - exact (Z.divide_add_cancel_r _ _ _ H).
  - exact (Z.divide_add_r _ _ _ H).
Qed.

Lemma divide_add_cancel_r (n m p : Z) : (n | m) -> (n | p + m) <-> (n | p).
Proof. rewrite Z.add_comm; exact (divide_add_cancel_l _ _ _). Qed.

(* Equivalence in Gauss lemma *)
Lemma divide_mul_cancel_l (n m p : Z) :
  Z.gcd n m = 1 -> (n | m * p) <-> (n | p).
Proof.
  intros H; split.
  - intros H'; exact (Z.gauss _ _ _ H' H).
  - clear H; exact (Z.divide_mul_r _ _ _).
Qed.

Lemma divide_mul_cancel_r (n m p : Z) :
  Z.gcd n m = 1 -> (n | p * m) <-> (n | p).
Proof. rewrite Z.mul_comm; exact (divide_mul_cancel_l _ _ _). Qed.

End Z.
