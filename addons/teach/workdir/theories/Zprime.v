(**
This file is part of the coq-teach library

Copyright (C) Boldo, Clément, Hamelin, Mayero, Rousselin

This library is free software; you can redistribute it and/or
modify it under the terms of the GNU Lesser General Public
License as published by the Free Software Foundation; either
version 3 of the License, or (at your option) any later version.

This library is distributed in the hope that it will be useful,
but WITHOUT ANY WARRANTY; without even the implied warranty of
MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the
COPYING file for more details.
*)

From Coq Require Import ZArith.BinInt Znumtheory.
From Teach Require Import Zmod_div Pos_prime Pos_Compl.

(** * Additional lemmas and functions about primality in Z *)

(** Remark: the definition of the [prime] predicate (Prop-valued) is in 
   ZArith.Znumtheory. It is quite surprising and not very practical. *)

Module Z.
(* Alternative (better) definitions of prime *)
Lemma prime_no_strict_divisor (p : Z) :
  prime p <-> 1 < p /\ forall a, (a | p) -> a = -1 \/ a = 1 \/ a = p \/ a = - p.
Proof.
  split; intros H.
  - pose proof H as [H1 H2]; split; [exact H1 |].
    exact (Znumtheory.prime_divisors p H).
  - destruct H as [H1 H2]; constructor; [exact H1 |].
    intros n Hn; apply Znumtheory.Zgcd_1_rel_prime.
    apply Z.le_antisymm; cycle 1;
      [apply Z.gcd_ge_1; right; apply Z.gt_1_neq_0; exact H1 |].
    apply Z.le_ngt; intros H.
    pose proof (Z.gcd_divide_r n p) as Hdiv.
    pose proof (Z.gcd_nonneg n p) as Ig.
    pose proof (Z.gcd_divide_l n p) as Hdivn.
    specialize (H2 _ Hdiv) as [C | [C | [C | C]]].
    + rewrite C in Ig; apply Z.le_ngt in Ig; now apply Ig.
    + rewrite C in H; apply (Z.lt_irrefl 1); exact H.
    + rewrite C in Hdivn.
      apply Z.divide_pos_le, Z.le_ngt in Hdivn; [| now apply Z.ge_1_gt_0, Hn].
      now apply Hdivn, Hn.
    + rewrite C in Ig; apply Z.le_ngt in Ig.
      apply Ig, Z.opp_neg_pos, Z.gt_1_gt_0; exact H1.
Qed.

Lemma prime_no_strict_divisor_below (p : Z) :
  prime p <-> 1 < p /\ forall a, 1 < a < p -> ~ (a | p).
Proof.
  split.
  - intros H; pose proof H as [H1 H2]; split; [exact H1 |]; intros a Ha Hdiv.
    apply prime_no_strict_divisor in H as [_ H].
    specialize (H a Hdiv) as [C | [C | [C | C]]]; rewrite C in Ha.
    + destruct Ha as [Ha _]; now apply (Z.lt_asymm (-1) 1).
    + destruct Ha as [Ha _]; apply (Z.lt_irrefl 1); exact Ha.
    + destruct Ha as [_ Ha]; apply (Z.lt_irrefl p); exact Ha.
    + destruct Ha as [Ha _]; apply (Z.lt_asymm (- p) 1); [| exact Ha].
      now apply Z.lt_trans with (2 := Z.lt_0_1), Z.opp_neg_pos, Z.gt_1_gt_0.
  - intros [Ip Hp]; apply prime_no_strict_divisor; split; [exact Ip |].
    intros a Hdiv; enough (Z.abs a = 1 \/ Z.abs a = p) as C. {
      destruct C as [C | C].
      + destruct (Z.abs_or_opp_abs a) as [C' | C']; [right; left | left];
          rewrite C'; [exact C |]; rewrite C; exact (Pos2Z.opp_pos _).
      + right; right; destruct (Z.abs_or_opp_abs a) as [C' | C'];
          [left | right]; rewrite C', C; reflexivity.
    }
    apply Z.divide_abs_l in Hdiv.
    pose proof (Z.abs_nonneg a) as [I1 | E1]%Z.le_lteq; cycle 1. {
      exfalso; rewrite <-E1 in Hdiv; apply Z.divide_0_l in Hdiv;
      rewrite Hdiv in Ip; apply (Z.lt_asymm 0 1 (Z.lt_0_1)); exact Ip.
    }
    pose proof (Z.gt_1_gt_0 p Ip) as Hpgt0.
    pose proof (Z.divide_pos_le (Z.abs a) p Hpgt0 Hdiv) as I2.
    assert (Z.abs a = 1 \/ 1 < Z.abs a) as [Ha | Ha]. {
      apply Z.le_succ_l in I1; rewrite <-Z.one_succ in I1.
      apply Z.le_lteq in I1 as [H | H]; [right | left; symmetry]; exact H.
    }
    + left; exact Ha.
    + apply Z.le_lteq in I2 as [I2 | E2]; [| right; exact E2].
      exfalso; apply (Hp (Z.abs a)); [| exact Hdiv]; split; assumption.
Qed.

Lemma divisor_le_sqrt (n m p : Z) :
  0 <= n -> n = m * p -> m <= Z.sqrt n \/ p <= Z.sqrt n.
Proof.
  intros H1 Hdiv.
  destruct (Z.le_ge_cases m 0) as [Im | Im]. {
    left; apply Z.le_trans with (1 := Im); exact (Z.sqrt_nonneg _).
  }
  destruct (Z.le_ge_cases p 0) as [Ip | Ip]. {
    right; apply Z.le_trans with (1 := Ip); exact (Z.sqrt_nonneg _).
  }
  destruct (Z.le_ge_cases m p) as [Imp | Imp]; [left | right].
  - apply Z.sqrt_le_square; [exact H1 | exact Im |].
    apply Z.mul_le_mono_nonneg_l with (1 := Im) in Imp; rewrite <-Hdiv in Imp;
    exact Imp.
  - apply Z.sqrt_le_square; [exact H1 | exact Ip |].
    apply Z.mul_le_mono_nonneg_r with (1 := Ip) in Imp; rewrite <-Hdiv in Imp;
    exact Imp.
Qed.

Lemma prime_no_strict_divisor_below_sqrt (p : Z) :
  prime p <-> 1 < p /\ forall a, 1 < a <= Z.sqrt p -> ~ (a | p).
Proof.
  split.
  - intros [Ip Hp]%prime_no_strict_divisor_below; split; [exact Ip |].
    intros a [Ia Ia']; apply Hp; split; [exact Ia |].
    apply Z.le_lt_trans with (1 := Ia').
    apply Z.sqrt_lt_lin; exact Ip.
  - intros [Ip Hp]; apply prime_no_strict_divisor_below; split; [exact Ip |].
    intros a [Ia Ia'] [q Hq].
    pose proof Hq as Hdiv; apply divisor_le_sqrt in Hdiv; cycle 1. {
      apply Z.lt_le_incl, Z.gt_1_gt_0; exact Ip.
    }
    destruct Hdiv as [Hdiv | Hdiv].
    + assert (1 < q) as Iq. {
        apply Z.lt_nge; intros C.
        apply (Z.mul_le_mono_nonneg_r _ _ a) in C; cycle 1. {
          apply Z.lt_le_incl, Z.lt_trans with (1 := Z.lt_0_1); exact Ia.
        }
        rewrite <-Hq, Z.mul_1_l in C; apply Z.le_ngt in C; apply C; exact Ia'.
      }
      pose proof (conj Iq Hdiv) as Iq'; specialize (Hp _ Iq'); apply Hp.
      exists a; rewrite Z.mul_comm; exact Hq.
    + pose proof (conj Ia Hdiv) as Ia''; specialize (Hp _ Ia''); apply Hp.
      exists q; exact Hq.
Qed.

Definition primeb (n : Z) : bool :=
  match n with
  | Z.neg p => false
  | 0 => false
  | Z.pos p =>
    match p with
    | 1%positive => false
    | _ => Pos.primeb_pos_aux p (Z.to_pos (Z.sqrt (Z.pos p)))
    end
  end.

Lemma Private_test_primeb :
  primeb (- 5)  = false /\
  primeb (- 4)  = false /\
  primeb (- 3)  = false /\
  primeb (- 2)  = false /\
  primeb (- 1)  = false /\
  primeb 0      = false /\
  primeb 1      = false /\
  primeb 2      = true  /\
  primeb 3      = true  /\
  primeb 4      = false /\
  primeb 5      = true  /\
  primeb 6      = false /\
  primeb 7      = true  /\
  primeb 8      = false /\
  primeb 9      = false /\
  primeb 10     = false /\
  primeb 11     = true  /\
  primeb 12     = false /\
  primeb 13     = true  /\
  primeb 14     = false /\
  primeb 15     = false /\
  primeb 16     = false /\
  primeb 17     = true  /\
  primeb 18     = false /\
  primeb 19     = true  /\
  primeb 65537  = true  /\
  primeb 65533  = false /\
  primeb 939391 = true. (* /\
  primeb 479001599 = true. *)
Proof. repeat split; reflexivity. Qed.

Lemma primeb_correct (n : Z) : primeb n = true <-> prime n.
Proof.
  unfold primeb; split; destruct n as [| p | p].
  - intros H; discriminate H. 
  - enough (forall p, (p <> 1)%positive ->
      Pos.primeb_pos_aux p (Z.to_pos (Z.sqrt (Z.pos p))) = true ->
      prime (Z.pos p)) as key. {
      destruct p as [p' | p' |]; [| | intros H; discriminate H]; apply key;
        unfold Pos.le; intros H; discriminate H.
    }
    intros q Hq.
    intros H; rewrite Pos.primeb_pos_aux_correct in H. (* apply fails *)
    apply Z.prime_no_strict_divisor_below_sqrt; split. {
      apply Pos2Z.pos_lt_pos, Pos.neq_1_lt_1; exact Hq.
    }
    intros a [Ha Ha']; intros Hdiv%Z.mod_divide%Z.eqb_eq.
    destruct a as [| a | a]; [discriminate Ha | | discriminate Ha].
    lapply (H a); [rewrite Hdiv; intros contra; discriminate contra |].
    split.
    + rewrite Pos.two_succ; apply Pos.le_succ_l; exact Ha.
    + exact Ha'.
  - intros H; discriminate H.
  - intros [H _]; easy.
  - enough (forall p, (p <> 1)%positive -> prime (Z.pos p) ->
      Pos.primeb_pos_aux p (Z.to_pos (Z.sqrt (Z.pos p))) = true) as key. {
      now intros Hp; destruct p as [p | p |];
        [apply key.. | apply not_prime_1 in Hp].
    }
    intros q Hq Pq; apply Pos.primeb_pos_aux_correct; intros n Hn.
    apply Z.prime_no_strict_divisor_below_sqrt in Pq as [_  Pq].
    apply Z.eqb_neq; intros Hdiv%Z.mod_divide.
    specialize (Pq (Z.pos n)); apply Pq; [| exact Hdiv].
    destruct Hn as [H2%Pos2Z.pos_le_pos Hsqrt%Pos2Z.pos_le_pos].
    split.
    * apply Z.le_succ_l; rewrite <-Z.two_succ; exact H2.
    * rewrite Z2Pos.id in Hsqrt; [exact Hsqrt | now apply Z.sqrt_pos].
  - now intros [I _]; contradict I.
Qed.
End Z.
