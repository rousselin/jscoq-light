{
  inputs = {
    nixpkgs.url = "nixpkgs/nixos-unstable";
    flake-utils.url = "github:numtide/flake-utils";
  };
  
  outputs = { self, nixpkgs, flake-utils, ... }: flake-utils.lib.eachDefaultSystem (system:
    let
      pkgs = nixpkgs.legacyPackages.${system};
    in {
      devShell = pkgs.mkShell {
        packages = with pkgs; [
          (vscode-with-extensions.override {
            vscodeExtensions = with vscode-extensions.ms-python; [
              python
              debugpy
              vscode-pylance
            ];
          })
        ];
      };
    });
}

